import logo from './logo.svg';
import './App.css';
import data from './data';
import {useState} from 'react'


function App() {

  const [size, setSize] = useState();
  const [text, setText] = useState([]);

  const handleSubmit = (e) => {
    e.preventDefault();
    console.log(size);
    setText(data);
  }

  return (
    
    <div className="App">
     <h3>Генератор текста</h3>

     <form onSubmit={handleSubmit}>
      <label>
        Параграфы
        <input type="number" value={size} onChange={(e) => setSize(e.target.value)}></input>
      </label>
      <button type="submit">сгенерировать</button>
     </form>
     <div>
       {text.map((item,index) => {
         return <p key={index}>{item}</p>
       })}
     </div>
    </div>
  );
}

export default App;
