import React, {useState, useEffect} from 'react'
import CreateTask from './CreateTask'
import TaskList from './TaskList';


export default function Main() {

    const [tasks, setTasks] = useState([]);

    const createTask = (task) => {
        if(task.trim() === ''){
            alert("Поле не должно быть пустым")
            return
        }else {
            setTasks([...tasks, {task, isComplete: false}])
        }
    }

    const deletaTask = (itemId) => {
        tasks.splice(itemId, 1);
        setTasks([...tasks]);
    }
    console.log(tasks);

    return (
        <div>
            <h3>Список задач</h3>

            <div>
                <CreateTask createTask={createTask} />
            </div>

            <div>
                <TaskList tasks={tasks}  deletaTask={deletaTask}/>
            </div>
        </div>
    )
}
