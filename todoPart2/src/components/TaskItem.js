import React from 'react'

export default function TaskItem({taskItem, id, deletaTask}) {

    const removeTask = () => {
        deletaTask(id)
    }

    return (
        <tr>
            <td>
                {taskItem.task}
            </td>
            <td>
                <button onClick={removeTask}>Удалить</button>
                <button>Редактировать</button>
            </td>
        </tr>
    )
}
