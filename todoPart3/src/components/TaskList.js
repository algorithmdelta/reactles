import React from 'react'
import TaskItem from './TaskItem'

export default function TaskList({tasks, deletaTask, SaveChange}) {


    return (
        <table>
            <thead>
                <tr>
                    <th>
                        Задачи
                    </th>
                    <th>
                        Действия
                    </th>
                </tr>
                </thead>
                <tbody>
                    {tasks.map((task, index) => (
                        <TaskItem key={index} taskItem={task} id={index} deletaTask={deletaTask} SaveChange={SaveChange}/>
                    ))}
                </tbody>

        </table>
    )
}
