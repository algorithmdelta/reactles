import React, {useState} from 'react'

export default function TaskItem({taskItem, id, deletaTask, SaveChange}) {

    const [task, setTask] = useState(taskItem.task);
    const [isEditing, setIsEditing]  = useState(false);


    const removeTask = () => {
        deletaTask(id)
    }

    const handleChange = (e) => {
        setTask(e.target.value)
    }
    const SaveEdit = () => {
        SaveChange(id, task)
        setIsEditing(false)
    }

    return (
        <tr>

            {isEditing ? 
            <>
                <td>
                    <form>
                        <input value={task} onChange={handleChange} autoFocus/>
                    </form>
                </td>
                <td>
                    <button onClick={SaveEdit}>Сохранить</button>
                    <button onClick={() => (setIsEditing(false))}>Отмена</button>
                </td>
            </>
            :
            <>
                <td>
                {taskItem.task}
                </td>
                <td>
                    <button onClick={removeTask}>Удалить</button>
                    <button onClick={() => (setIsEditing(true))}>Редактировать</button>
                </td>
            </>
            
        }
            
        </tr>
    )
}
