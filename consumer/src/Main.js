import React from 'react'
import Menu from './Menu'
import { ThemeContextConsumer } from './ThemeContext'

export default function Main(props) {
    return (
        <ThemeContextConsumer>
        {(context) => (
            <div>
                <div className={context.theme}>
                <Menu />
                 </div>  

                 <div>
                     {props.children}
                 </div>
            </div>  
        )}

    </ThemeContextConsumer>
        
    )
}
