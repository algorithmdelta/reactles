import React, {useState} from 'react'

const {Provider, Consumer} = React.createContext();

function ThemeContextProvider(props) {
    const [theme, setTheme] = useState("night")

    const toogleTheme = () => {
        setTheme(theme=== "night" ? "day" : "night");
    }

    return (
        <Provider value={{theme, toogleTheme}}>{props.children}</Provider>
    )
}

export {ThemeContextProvider, Consumer as ThemeContextConsumer}
