import React from 'react'
import {NavLink} from 'react-router-dom'

export default function Menu() {
    return (
        <div>
            <NavLink to="/">Главная</NavLink>
            <NavLink to="/about">О компании</NavLink>
            <NavLink to="/contact">Контакты</NavLink>
        </div>
    )
}
