import logo from './logo.svg';
import './App.css';
import {Switch, BrowserRouter, Route} from 'react-router-dom'
import Main from './components/Main';
import Conact from './components/Conact';
import About from './components/About';

function App() {
  return (
    <BrowserRouter>
      <Switch>
        
        <Route path="/" exact component={Main}/>
        <Route path="/about" exact component={About}/>
        <Route path="/contact" exact component={Conact}/>

      </Switch>
    </BrowserRouter>

  );
}

export default App;
