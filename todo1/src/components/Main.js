import React, {useState} from 'react'
import CreateTask from './CreateTask'


export default function Main() {

    const [tasks, setTasks] = useState([]);

    const createTask = (task) => {
        if(task.trim() === ''){
            alert("Поле не должно быть пустым")
            return
        }else {
            setTasks([...tasks, {task, isComplete: false}])
        }
    }
    console.log(tasks);

    return (
        <div>
            <h3>Список задач</h3>

            <div>
                <CreateTask createTask={createTask} />
            </div>

            <div>
                Список из заданий
            </div>
        </div>
    )
}
