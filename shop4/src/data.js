const data = {
    products : [
        {
            id:1,
            name:"ZAZOU",
            price:"90500",
            Image:'boat1.png',
            text:"Построенный для опытного постоянного клиента Benetti, который теперь переезжает на новый 50-метровый"
        },
        {
            id:2,
            name:"ILLUSION PLUS",
            price:"80250",
            Image:'boat2.jpg',
            text:"ILLUSION PLUS с ее впечатляющей голландской военно-морской архитектурой от Azure и Rainsford Saunders сочетает"
        },
        {
            id:3,
            name:"SKAT",
            price:"49000",
            Image:'boat3.png',
            text:"SKAT, что в переводе с датского означает «сокровище», - это революционная яхта, состоящая из углов и острых граней"
        },
        {
            id:4,
            name:"Лодка рыбачок",
            price:"300",
            Image:'boat4.png',
            text:"Отличная лодка позволит Вам заплывать в любые уголки любимого озера, и ловить большую рыбу без проблем!"
        },

    ]
}

export default data;