import logo from './logo.svg';
import './App.css';
import Header from './Components/Header';
import Main from './Components/Main';
import Cart from './Components/Cart';
import data from './data';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Row, Col} from 'react-bootstrap'
import {useState} from "react"


function App() {

  const {products} = data;
  const [cartItems, setCartItems] = useState([]);

  const onAdd = (product) => {
    const exist = cartItems.find((x) => x.id === product.id);
    if(exist) {
      setCartItems(cartItems.map((x) => 
        x.id === product.id ? {...exist, qty: exist.qty + 1} : x
      ))
    } else {
      setCartItems([...cartItems, {...product, qty: 1}]);
    }
  }

  const onRemove = (product) => {
    const exist = cartItems.find((x) => x.id === product.id);
    if(exist.qty === 1) {
      setCartItems(cartItems.filter((x) => x.id !==product.id))
    }else {
      setCartItems(cartItems.map((x) => 
      x.id === product.id ? {...exist, qty: exist.qty - 1} : x
    ))
    }
  }

  return (
    <div className="App">
        <Header count={cartItems.length} />
        <Row>
          <Col md={8}>
            <Main onAdd={onAdd} products={products} />
          </Col>

          <Col>
            <Cart onAdd={onAdd} onRemove={onRemove} cartItems={cartItems} />
          </Col>
        </Row>
         
          
    </div>
  );
}

export default App;
