import React from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import {Navbar, Nav, Container} from 'react-bootstrap'

export default function Header({count}) {
    return (
        <div>
            <Navbar bg="primary" expand="lg" variant="dark" className="mb-4">
            <Container>
                <Navbar.Brand href="#home">Наш магазин</Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="me-auto">
                    <Nav.Link href="#home">Корзина 

                    {count ? (<span className="count"> {count}</span>) : ("")}
                    
                    </Nav.Link>
                
                </Nav>
                </Navbar.Collapse>
            </Container>
            </Navbar>
        </div>
    )
}
