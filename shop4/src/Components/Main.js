import React from 'react'
import Product from './Product'
import {Row} from 'react-bootstrap'

export default function Main(props) {

    const {products, onAdd} = props
    return (
        <Row>
            {products.map((product) => (
                <Product key={product.id} product={product} onAdd={onAdd}></Product>
            ))}
        </Row>
    )
}
