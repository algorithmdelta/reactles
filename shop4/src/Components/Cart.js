import React from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import {Alert} from 'react-bootstrap'
import { Button } from 'react-bootstrap';

export default function Cart(props) {

    const {cartItems, onAdd, onRemove} = props;

    const totalPrice = cartItems.reduce((a, c) => a + c.price * c.qty, 0);

    const finale = () => {
       alert("Это учебный сайт, благодарим за внимание!")
    }
    return (
        <div>
            <Alert variant="primary">
                <Alert.Heading>Корзина</Alert.Heading>
                <div>
                   {cartItems.length === 0 && <div> Корзина пуста </div>}
                </div>

                <div>
                   {cartItems.map((item)=> (
                       <div>
                        <span> {item.name} </span>
                        <Button onClick={()=> onAdd(item)}> + </Button>
                        <Button onClick={()=> onRemove(item)} variant="danger"> - </Button>
                        <span>ко-во {item.qty} </span>
                        <span>цена {item.price} </span>
                        <strong>всего {item.price * item.qty} </strong>
                       </div>
                   ))}
                </div>
                
                <hr />
                <p className="mb-0">
                    Общая сумма к оплате <strong>{totalPrice}</strong>
                </p>

                <Button onClick={finale}>Оформить заказ</Button>
                </Alert>
        </div>
    )
}
