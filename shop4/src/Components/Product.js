import React from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import {Card, Col, Button} from 'react-bootstrap'

export default function Product(props) {
    const {product, onAdd} = props
    return (
        <Col sm={6} md={3}>
            <Card style={{ maxWidth: '300px' }}>
                <Card.Img variant="top" src={product.Image} />
                <Card.Body>
                    <Card.Title>{product.name}</Card.Title>
                    <Card.Text>
                    {product.price} Евро
                    </Card.Text>
                    <Card.Text>
                    {product.text}
                    </Card.Text>
                    <Button onClick={()=> onAdd(product)}> Купить </Button>
    
                </Card.Body>
                </Card>
        </Col>
    )
}
