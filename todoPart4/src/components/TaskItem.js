import React, {useState} from 'react'

export default function TaskItem({taskItem, id, deletaTask, SaveChange, toogleTask}) {

    const [task, setTask] = useState(taskItem.task);
    const [isEditing, setIsEditing]  = useState(false);


    const removeTask = () => {
        deletaTask(id)
    }

    const handleChange = (e) => {
        setTask(e.target.value)
    }
    const toogle = () => {
        toogleTask(id)
    }
    const SaveEdit = () => {
        SaveChange(id, task)
        setIsEditing(false)
    }

    return (
        <tr>

            {isEditing ? 
            <>
                <td>
                    <form>
                        <input value={task} onChange={handleChange} autoFocus/>
                    </form>
                </td>
                <td>
                    <button className="button"  onClick={SaveEdit}>Сохранить</button>
                    <button className="button"  onClick={() => (setIsEditing(false))}>Отмена</button>
                </td>
            </>
            :
            <>
                <td onClick={toogle}>
                    <span className={taskItem.isComplete ? "complete": "not-complete"}>{taskItem.task}</span> 
                </td>
                <td>
                    <button className="button deleteBtn" onClick={removeTask}>Удалить</button>
                    <button className="button"  onClick={() => (setIsEditing(true))}>Редактировать</button>
                </td>
            </>
            
        }
            
        </tr>
    )
}
