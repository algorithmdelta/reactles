import React, {useState, useEffect} from 'react'
import CreateTask from './CreateTask'
import TaskList from './TaskList';


export default function Main() {

    const [tasks, setTasks] = useState([]);

    const createTask = (task) => {
        if(task.trim() === ''){
            alert("Поле не должно быть пустым")
            return
        }else {
            setTasks([...tasks, {task, isComplete: false}])
        }
    }

    const toogleTask = (itemId) => {
        const taskItem = tasks[itemId];
        taskItem.isComplete = !taskItem.isComplete;
        setTasks([...tasks]);
    }

    const deletaTask = (itemId) => {
        tasks.splice(itemId, 1);
        setTasks([...tasks]);
    }

    const SaveChange = (itemId, task) => {
        const taskItem = tasks[itemId];
        taskItem.task = task;
        setTasks([...tasks]);
    }
    console.log(tasks);

    return (
        <div className="main">
            <h3>Список задач</h3>

            <div>
                <CreateTask createTask={createTask} />
            </div>

            <div>
                <TaskList tasks={tasks}  deletaTask={deletaTask} SaveChange={SaveChange} toogleTask={toogleTask}/>
            </div>
        </div>
    )
}
