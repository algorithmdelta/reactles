import logo from './logo.svg';
import './App.css';
import Main from './Main';
import Info from './Info';



function App() {
  return (
     <Main> 

       <Info />
     </Main>
  );
}

export default App;
