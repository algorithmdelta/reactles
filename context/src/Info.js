import React from 'react'
import { ThemeContextConsumer } from './ThemeContext'

export default function Info() {
    return (
        <ThemeContextConsumer>
        {(context) => (
            <div className={`content ${context.theme}`}>
               <h2>Заголовок сайта</h2>

               <p>Описание сайта</p>
                
            </div>              
        )}

    </ThemeContextConsumer>
    )
}
