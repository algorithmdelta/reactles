import React from 'react'
import { ThemeContextConsumer } from './ThemeContext'

export default function Menu() {
    return (
        <ThemeContextConsumer>
            {(context) => (
                <div className="menu">
                    <div>
                        Название сайта
                    </div>
                    <div>
                         <button onClick={context.toogleTheme}>Переключить тему</button>
                    </div>
                    
                </div>              
            )}

        </ThemeContextConsumer>
    )
}
