import logo from './logo.svg';
import './App.css';
import {Navbar, Container, Nav, Button, Card, Row} from 'react-bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css';
import ProductCard from './ProductCard';

function App() {

  return (
    <div>
     
      <Navbar bg="secondary " variant="dark" expand="lg">
        <Container>
          <Navbar.Brand href="#home">My project</Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="me-auto">
              <Nav.Link href="#home">Главная</Nav.Link>
              <Nav.Link href="#link">О компании</Nav.Link>
              <Nav.Link href="#link">Услуги</Nav.Link>
              <Nav.Link href="#link">Контакты</Nav.Link>
             
            </Nav>
            <Nav>
                <Button className="m-2" variant="info">Регистрация</Button>
                <Button className="m-2" variant="outline-info">Вход</Button>
              </Nav>
          </Navbar.Collapse>
        </Container>
    </Navbar>

    
    <Row xs={2} md={4} lg={6}>
      <ProductCard title={"Заголовок 1"} info={"Описание продукта 1"} image={"mini.jpg"}/>

      <ProductCard title={"Заголовок 2"} info={"Описание продукта 2"} image={"bike.png"}/>

      <ProductCard title={"Заголовок 1"} info={"Описание продукта 1"} image={"mini.jpg"}/>

      <ProductCard title={"Заголовок 2"} info={"Описание продукта 2"} image={"bike.png"}/>

      <ProductCard title={"Заголовок 1"} info={"Описание продукта 1"} image={"mini.jpg"}/>

      <ProductCard title={"Заголовок 2"} info={"Описание продукта 2"} image={"bike.png"}/>

      <ProductCard title={"Заголовок 1"} info={"Описание продукта 1"} image={"mini.jpg"}/>

      <ProductCard title={"Заголовок 2"} info={"Описание продукта 2"} image={"bike.png"}/>
    </Row>
      
      
    
    
     
    </div>
  );
}

export default App;
