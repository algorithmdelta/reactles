import React from 'react'
import {Card, Button} from 'react-bootstrap'

export default function ProductCard({title, info, image}) {
    return (
        <div>
            <Card className="m-3 p-3" style={{ width: '250px' }}>
                <Card.Img variant="top" src={image} />
                <Card.Body>
                    <Card.Title>{title} </Card.Title>
                    <Card.Text>
                        {info}
                    </Card.Text>
                    <Button variant="secondary">Подробнее</Button>
                </Card.Body>
              </Card>
        </div>
    )
}
